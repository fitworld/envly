const projectRepository = require("./project");
const enviromentRepository = require("./enviroment");
const variablesRepository = require("./variables");
const fs = require("fs");
const os = require("os");

function deploy(projectName, enviromentName, cmdObj) {
    // Validate existence of project
    const project = projectRepository.get(projectName);
    if (project == null) return null;
    // Validate existence of enviroment
    const enviroment = enviromentRepository.get(enviromentName);
    if (enviroment == null) return null;

    const variables = variablesRepository.getAllForEnviroment(projectName, enviromentName);

    // Construct key/value pairs
    let str = "";
    for (let i = 0; i < variables.length; i++) {
        const variable = variables[i];
        str += variable.name + "=" + variable.value;
        if (i !== (variables.length - 1)) {
            str += os.EOL;
        }
    }

    // Maybe make this dynamic, depending on an option?
    if (fs.existsSync(".env")) {
        console.log("removing existing .env");
        fs.unlinkSync(".env");
    }

    console.log("writing new .env");
    fs.writeFileSync(".env", str);

    console.log("successfully deployed")
}

module.exports = {
    deploy: deploy
};
