const deploy = require("./deploy");

module.exports = program => {
    program
        .command('deploy <projectName> <enviromentName>')

        // TODO implement, too much complexity since we'd have to save the existing .env en do diff concilation
        // TODO otherwise we'd end up with a huge .env after a few deployments with multiple duplicates
        // (option is available in cmdObj.nodestroy, if the flag is present), otherwise is undefined (instead of false, duh)
        // .option("-n, --nodestroy", "don't destroy the .env file. WARNING: this might cause duplicate variable declarations in .env if you're not careful.")

        .description("destroys an existing .env and replaces the values within")
        .action(deploy.deploy);
};
