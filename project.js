const db = require("./db");
const shortid = require("shortid");

function edit(oldName, newName) {
    const project = db
        .get('projects')
        .find({name: oldName})
        .value();
    if (project == null) return console.log("err: project not found");
    db.get('projects')
        .find({id: project.id})
        .assign({name: newName})
        .write();
    console.log("successfully edited project from " + oldName + " to " + newName)
}

function add(name) {
    // unique project names
    const projects = db.get('projects')
        .find({name: name})
        .value();
    if (projects != null) return console.log("err: project already exists");

    db.get("projects")
        .push({
            id: shortid.generate(),
            name: name
        })
        .write();
    console.log("successfully added project " + name);
}

function remove(name) {
    const project = db
        .get('projects')
        .find({name: name})
        .value();
    if (project == null) return console.log("err: project not found");

    db.get("projects")
        .remove({name: name})
        .write();
    console.log("successfully removed project " + name)
}

function get(name) {
    const project = db
        .get('projects')
        .find({name: name})
        .value();
    if (project == null) return console.log("err: project not found");
    return project;
}

function getAll(opts) {
    const projects = db
        .get('projects')
        .value();
    return projects;
}

module.exports = {
    edit: edit,
    add: add,
    remove: remove,
    get: get,
    getAll: getAll,
};
