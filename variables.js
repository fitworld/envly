const db = require("./db");
const projectRepository = require("./project");
const enviromentRepository = require("./enviroment");
const shortid = require("shortid");

// structure
const bla = {
    id: "wadw",
    name: "wadawd",
    value: "wakdjwakdjw",
    enviroment: "wdjkw290283", // ID
    project: "wkjhd292983" // ID
};

function get(variableName, enviromentName, projectName) {
    // Null checks for dependencies
    const project = projectRepository.get(projectName);
    if (project == null) return console.log("couldn't find an enviroment variable with that project connected to it");
    const enviroment = enviromentRepository.get(enviromentName);
    if (enviroment == null) return console.log("couldn't find an enviroment variable with that enviroment added to it");

    const variable = db
        .get('variables')
        .find({
            name: variableName,
            project: project.id,
            enviroment: enviroment.id
        })
        .value();
    if (variable == null) return console.log("err: variable not found");
    return variable;
}

function getAll(projectName) {
    // Null checks for dependencies
    const project = projectRepository.get(projectName);
    if (project == null) return console.log("couldn't find an enviroment variable with that project connected to it");
    const variables = db
        .get('variables')
        .find({
            project: project.id,
        })
        .value();
    if (variables == null) return console.log("err: variables not found");
    return variables;
}

function getAllForEnviroment(projectName, enviromentName) {
    // Null checks for dependencies
    const project = projectRepository.get(projectName);
    if (project == null) return console.log("couldn't find an enviroment variable with that project connected to it");
    const enviroment = enviromentRepository.get(enviromentName);
    if (enviroment == null) return null;
    const variables = db
        .get('variables')
        .chain()
        .filter({
            project: project.id,
            enviroment: enviroment.id
        })
        .value();
    if (variables == null) return console.log("err: variables not found");
    return variables;
}

function add(variableName, variableValue, enviromentName, projectName) {
    // Null checks for dependencies
    const project = projectRepository.get(projectName);
    if (project == null) return console.log("couldn't find an enviroment variable with that project connected to it");
    const enviroment = enviromentRepository.get(enviromentName);
    if (enviroment == null) return console.log("couldn't find an enviroment variable with that enviroment added to it");

    const existing = db.get('variables')
        .chain()
        .filter({
            name: variableName,
            enviroment: enviroment.id,
            project: project.id
        })
        .value();

    if (existing != null && existing.length > 0) return console.log("err: variable already exists for that project/env combination");

    const obj = {
        id: shortid.generate(),
        name: variableName,
        value: variableValue,
        enviroment: enviroment.id,
        project: project.id
    };
    db.get('variables')
        .push(obj)
        .write();


}

function editValue(variableName, variableNewValue, enviromentName, projectName) {
    // Null checks for dependencies
    const project = projectRepository.get(projectName);
    if (project == null) return console.log("couldn't find an enviroment variable with that project connected to it");
    const enviroment = enviromentRepository.get(enviromentName);
    if (enviroment == null) return console.log("couldn't find an enviroment variable with that enviroment added to it");

    const variable = db
        .get('variables')
        .find({
            name: variableName,
            project: project.id,
            enviroment: enviroment.id
        })
        .value();
    if (variable == null) return console.log("err: variable not found");

    db
        .get('variables')
        .find({id: variable.id})
        .assign({value: variableNewValue})
        .write();

    console.log("successfully edited variable")
}

function editRename(variableName, variableNewName, enviromentName, projectName) {
    // Null checks for dependencies
    const project = projectRepository.get(projectName);
    if (project == null) return console.log("couldn't find an enviroment variable with that project connected to it");
    const enviroment = enviromentRepository.get(enviromentName);
    if (enviroment == null) return console.log("couldn't find an enviroment variable with that enviroment added to it");

    const variable = db
        .get('variables')
        .find({
            name: variableName,
            project: project.id,
            enviroment: enviroment.id
        })
        .value();
    if (variable == null) return console.log("err: variable not found");

    db
        .get('variables')
        .find({id: variable.id})
        .assign({name: variableNewName})
        .write();

    console.log("successfully edited variable")
}

function remove(variableName, enviromentName, projectName) {
    // Null checks for dependencies
    const project = projectRepository.get(projectName);
    if (project == null) return console.log("couldn't find an enviroment variable with that project connected to it");
    const enviroment = enviromentRepository.get(enviromentName);
    if (enviroment == null) return console.log("couldn't find an enviroment variable with that enviroment added to it");

    const variable = db
        .get('variables')
        .find({
            name: variableName,
            project: project.id,
            enviroment: enviroment.id
        })
        .value();
    if (variable == null) return console.log("err: variable not found");

    db
        .get('variables')
        .remove({id: variable.id})
        .write();

    console.log("successfully removed variable")
}

module.exports = {
    get: get,
    getAll: getAll,
    getAllForEnviroment: getAllForEnviroment,
    add: add,
    editValue: editValue,
    editRename: editRename,
    remove: remove
};
