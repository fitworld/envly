#!/usr/bin/env node

const program = require('commander');
const cliProject = require("./cli-project");
const cliEnviroment = require("./cli-enviroment");
const cliVariables = require("./cli-variables");
const cliDeploy = require("./cli-deploy");
const fs = require("fs");

// TODO edit help logging of commands in subgroups
// TODO add ability to add an enviroment to a project (so that a project has an array of enviroment IDs)
// TODO add ability to add variables to enviroments (with IDs)

// TODO dot-prop and check for required options (commander is shit)
// TODO use os.EOL when constructing .env file

program.name("envly");
program.usage("envly <command> <values>");

// add deployment commands (replacing of .env)
cliDeploy(program);
// add project commands
cliProject(program);
// add enviroment commands
cliEnviroment(program);
// add variables-enviroment commands
cliVariables(program);

program.parse(process.argv);

if (program.debug) console.log(program.opts());
