# Envly

A semi-automated way of managing your .env files across enviroments.


## Quick start
Use ``npx envly deploy <projectName> <enviromentName>`` to write a .env to the current working directory of your shell.
````
npx envly --help
````

## Why?
All the tools that existed were overly complicated or required money. Simplicity sometimes is nice.

## Help! Something exploded!
Please submit what happened and steps to reproduce at [the issues page](https://gitlab.com/fitworld/envly/issues)

## Questions
Submit an issue [the issues page](https://gitlab.com/fitworld/envly/issues) and I'll answer ASAP.
