const project = require("./project");

module.exports = program => {
    // Project related CRUD actions

    program.command('add-project <name>')
        .description('adds a project')
        .action(project.add);

    program.command('edit-project <oldName> <newName>')
        .description('edit a project')
        .action(project.edit);

    program.command('remove-project <name>')
        .description('removes a project')
        .action(project.remove);

    program.command('get-project <name>')
        .description('gets a project')
        .action(project.get);

    program.command('get-all-projects')
        .description('gets all projects')
        .action(project.getAll);

};
