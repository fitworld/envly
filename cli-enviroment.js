const enviroment = require("./enviroment");

module.exports = program => {
    // enviroment related CRUD actions

    program.command('add-enviroment <name>')
        .description('adds an enviroment')
        .action(enviroment.add);

    program.command('edit-enviroment <oldName> <newName>')
        .description('edit an enviroment')
        .action(enviroment.edit);

    program.command('remove-enviroment <name>')
        .description('removes an enviroment')
        .action(enviroment.remove);

    program.command('get-enviroment <name>')
        .description('gets an enviroment')
        .action(enviroment.get);

    program.command('get-all-enviroments')
        .description('gets all enviroments')
        .action(enviroment.getAll);
};
