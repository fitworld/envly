const variables = require("./variables");

module.exports = program => {
    program
        .command('add-variable <variableName> <variableValue> <enviromentValue> <projectName>')
        .description('add a variable to a project')
        .action(variables.add);
    program
        .command('edit-rename-variable <variableName> <variableNewName> <enviromentName> <projectName>')
        .description("edits a variable's value")
        .action(variables.editRename);
    program
        .command('edit-change-variable <variableName> <variableNewValue> <enviromentName> <projectName>')
        .description("edits a variable's value")
        .action(variables.editValue);
    program
        .command('remove-variable <variableName> <enviromentName>  <projectName>')
        .description("removes a variable value")
        .action(variables.remove);
    program
        .command('get-variable <variableName> <enviromentName> <projectName>')
        .description("gets variable key/value")
        .action(variables.get);
    program
        .command('get-all-variables <projectName>')
        .description("gets all variables for project")
        .action(variables.getAll);
    program
        .command('get-all-variables-enviroment <projectName> <enviromentName>')
        .description('gets all variables for that project and enviroment')
        .action(variables.getAllForEnviroment);
};
