const db = require("./db");
const shortid = require("shortid");

function edit(oldName, newName) {
    const enviroment = db
        .get('enviroments')
        .find({name: oldName})
        .value();
    if (enviroment == null) return console.log("err: enviroment not found");
    db.get('enviroments')
        .find({id: enviroment.id})
        .assign({name: newName})
        .write();
    console.log("successfully edited enviroment from " + oldName + " to " + newName)
}

function add(name) {
    // unique enviroment names
    const enviroments = db.get('enviroments')
        .find({name: name})
        .value();
    if (enviroments != null) return console.log("err: enviroment already exists");

    db.get("enviroments")
        .push({
            id: shortid.generate(),
            name: name
        })
        .write();
    console.log("successfully added enviroment " + name);
}

function remove(name) {
    const enviroment = db
        .get('enviroments')
        .find({name: name})
        .value();
    if (enviroment == null) return console.log("err: enviroment not found");

    db.get("enviroments")
        .remove({name: name})
        .write();
    console.log("successfully removed enviroment " + name)
}

function get(name) {
    const enviroment = db
        .get('enviroments')
        .find({name: name})
        .value();
    if (enviroment == null) return console.log("err: enviroment not found");
    return enviroment;
}

function getAll(opts) {
    const enviroments = db
        .get('enviroments')
        .value();
    return enviroments;
}

module.exports = {
    edit: edit,
    add: add,
    remove: remove,
    get: get,
    getAll: getAll,
};
