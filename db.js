const os = require("os");
const path = require("path");
const fs = require("fs");
const lowdb = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");

const configAddress = path.join(os.homedir(), ".envly");

try {
    fs.mkdirSync(configAddress);
} catch (err) {
    // ignore error if we can't make home dir, assume we already have it created
}

const dbAddress = path.join(configAddress, "db.json");
const adapter = new FileSync(dbAddress);
const db = lowdb(adapter);

db.defaults({
    projects: [],
    enviroments: [],
    variables: []
})
    .write();

module.exports = db;
